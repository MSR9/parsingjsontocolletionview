//
//  CustomCollectionViewCell.swift
//  ParsingJSONToCollectionView
//
//  Created by EPITADMBP04 on 3/20/20.
//  Copyright © 2020 PrahladReddy. All rights reserved.
//

import UIKit

class CustomCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageLbl: UILabel!
    
}
