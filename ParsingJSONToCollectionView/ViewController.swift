//
//  ViewController.swift
//  ParsingJSONToCollectionView
//
//  Created by EPITADMBP04 on 3/20/20.
//  Copyright © 2020 PrahladReddy. All rights reserved.
//

import UIKit
import Foundation

// URL: - "https://api.opendota.com/api/heroStats"

extension UIImageView {
    func downloadedFrom(url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) {data, response,error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
        }.resume()
    }

    func downloadedFrom(link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
}

struct Hero: Decodable {
    let localized_name: String
    let img: String
}

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var heros = [Hero]()
    
   
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.dataSource = self
        let url = URL(string: "https://api.opendota.com/api/heroStats")
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            if error == nil {
                do {
                    self.heros = try JSONDecoder().decode([Hero].self, from: data!)
                } catch {
                    print("Parse Error")
                }
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
            }
        }.resume()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection  section : Int) -> Int {
            return heros.count
        }
        
     func collectionView(_ collectonView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomCell", for: indexPath) as! CustomCollectionViewCell
        
        cell.imageLbl.text = heros[indexPath.row].localized_name.capitalized
        
        let defaultLink = "https://api.opendota.com"
         let completeLink = defaultLink + heros[indexPath.row].img
         cell.imageView.downloadedFrom(link: completeLink)
         cell.imageView.clipsToBounds = true
         cell.imageView.contentMode = .scaleToFill
         cell.imageView.layer.cornerRadius = cell.imageView.frame.height / 2
         return cell
        }
}


